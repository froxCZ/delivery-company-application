package com.wa2.frontend;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class FrontendApp {

    public static void main(String[] args) {
        new SpringApplicationBuilder(FrontendApp.class).run(args);
    }
}
