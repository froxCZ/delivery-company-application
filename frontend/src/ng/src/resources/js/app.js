(function () {
  'use strict';
  angular.module('messe',
    [
      'ui.router',
      'ui.bootstrap',
      'toaster', 'ngAnimate',

      'messe.util',
      'messe.consignment',
      'messe.operator',
      'messe.courier',
      'messe.user',
      'messe.navbar',
      'messe.websocket'

    ])
    .constant('BASE_URL', 'resources/app/')
    .constant('NODE_1', {url: 'http://localhost:8081/rest', id: 1})
    .constant('NODE_2', {url: 'http://localhost:8082/rest', id: 2})
    // .constant('API_REST', 'rest')
    .constant('LOGIN_ID', 'loginId')
    .constant('WS_URL_1', 'ws://localhost:8081/deliveries')
    .constant('WS_URL_2', 'ws://localhost:8082/deliveries')
    .factory('httpRequestInterceptor', function (storeService, LOGIN_ID) {
      return {
        request: function (config) {
          var loginId = storeService.get(LOGIN_ID);
          if (!loginId) {
            loginId = 2;
          }
          config.headers = {
            'X-LOGIN-ID': loginId,
            'Content-Type': 'application/json'
          };
          return config;
        }
      };
    })
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider, BASE_URL, $httpProvider) {
      $httpProvider.interceptors.push('httpRequestInterceptor');

      $stateProvider
        .state('app', {
          url: '/',
          abstract: true,
          templateUrl: BASE_URL + 'app.html',
          controller: 'MainController',
          resolve: {
            user: function (userService) {
              return userService.getMe();
            }
          }

        });

      $urlRouterProvider.otherwise("userprofile/");

    })
    .controller('MainController', function MainController(user, storeService, $http, NODE_1) {
      storeService.put('me', user.data);

      var node = storeService.get('node');
      if (!node || !node.id) {
        storeService.put('node', NODE_1);
      }
    });
})();
