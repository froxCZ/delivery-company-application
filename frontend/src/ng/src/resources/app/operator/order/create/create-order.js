(function() {

  var CreateOrderController = function(products, groups, orderService, $state, toaster) {

	this.products = products.data;
	this.groups = groups.data;
	  
    this.order = {
    	id : null,
    	code : new Date().getTime(),
    	product : this.products[0].id,
    	group : this.groups[0].id,
    	price : 0.0,
    	pickup : {
    		  name : "Jan Novák",
    		  telephone : "795 878 266",
    		  street : "Na lepším",
    		  houseNumber : "8",
    		  postalcode : "140 00",
    		  city : "Praha",
			  dateFrom : new Date(),
			  dateTo : new Date()
    	},
    	delivery : {
    		  name : "Milan Zelený",
    		  telephone : "420 779 897",
    		  street : "Thákurova",
    		  houseNumber : "9",
    		  postalcode : "160 00",
    		  city : "Praha",
			  dateFrom : new Date(),
			  dateTo : new Date()
    	},
    	packages : [{
    		weight : 1000,
    		height : 50,
    		width : 50,
    		depth : 50
    	}]
    };
    
    this.save = function() {
    	console.log("save order");
    	console.log(this.order);
		
        orderService.save(this.order).then(function() {
			$state.go('app.consignment');
        }, function errorCallback(response) {
        	var msg = response.data.indexOf("empty") >= 0 ? response.data : '';
        	toaster.pop('error', "Error", "Order wasn´t created. " + msg);
		  });
      
    };
    
  };

  angular.module('messe.operator.order.create',
    [
     	'messe.operator.order.form'
    ])
    .controller('CreateOrderController', CreateOrderController);

})();
