(function() {

    var ProductService = function($http, storeService) {
      var API_REST = storeService.get('node').url;

      this.getList = function() {
      return $http.get(API_REST + '/products');
    };
  };  
    
  var OrderService = function($http, storeService) {
    var API_REST = storeService.get('node').url;

    this.save = function(order) {
    	return $http.post(API_REST + '/orders', order);	
    };
    
    this.getList = function() {
        return $http.get(API_REST + '/orders');
    };
    
    this.getPrice = function(order){
    	return $http.get(API_REST + '/orders/computePrice', {
    	    params: { 
    	    	productId : order.product,
    	    	origins : order.pickup.street + " " + order.pickup.houseNumber + " " + order.pickup.city,
    	    	destinations : order.delivery.street + " " + order.delivery.houseNumber + " " +order.delivery.city
    	    }
    	});
    };

    this.getCourierOrdersList = function() {
        return $http.get(API_REST + '/orders/mine/undelivered/');
    };

    this.startDelivering = function(orderId) {
      return $http.post(API_REST + '/orders/' + orderId + '/startDelivering');
    };

    this.wasDelivered = function(orderId) {
      return $http.post(API_REST + '/orders/' + orderId + '/wasDelivered');
    };  
      
  };  

  angular.module('messe.operator.order')
    .service('productService', ProductService)
    .service('orderService', OrderService);
})();
