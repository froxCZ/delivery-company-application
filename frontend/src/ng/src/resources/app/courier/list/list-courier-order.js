(function () {

  var ListCourierOrderController = function (orders, orderService, deliveryService, $timeout, $scope, storeService, users) {
    var vm = this;
    vm.orders = orders.data;
    vm.users = users.data;
    vm.me = storeService.get('me');

    vm.startDelivering = function (orderId) {
      orderService.startDelivering(orderId);
      // deliveryService.sendUpdate();
    };

    vm.wasDelivered = function (orderId) {
      orderService.wasDelivered(orderId);
      // deliveryService.sendUpdate();
    };

    vm.isDelivering = function (orderState) {
      return orderState.toUpperCase() === 'delivering'.toUpperCase();
    };

    vm.onUpdate = function () {
      orderService.getCourierOrdersList().then(vm.processUpdate);
    };

    vm.processUpdate = function (orders) {
      console.log('processUpdate');
      // timeout zajisti, ze $apply probehne pri dalsim $digest a nevznikne tak konflikt
      $timeout(function () {
        vm.orders = orders.data;
        $scope.$apply();
      });
    };

    vm.isMine = function (order) {
      return +vm.me.id === +order.courier;
    };

    deliveryService.registerCallback(vm.onUpdate);

  };

  angular.module('messe.courier.list.orders',
    [])

    .controller('ListCourierOrderController', ListCourierOrderController);

})();