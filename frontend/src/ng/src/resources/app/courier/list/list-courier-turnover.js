(function() {

  var ListCourierTurnoverController = function(turnovers, turnoverService) {
	var vm = this; 
	this.turnovers = turnovers.data;
    vm.fromDate = "01.06.2016";
    vm.toDate = "30.06.2016";

    this.generate = function(){
    	turnoverService.generate(vm.fromDate,vm.toDate).then(function(response){
          setTimeout(function(){
            vm.turnovers = turnoverService.getList().then(function(response){
              vm.turnovers = response.data;
                console.log(vm.turnovers);
            });
          },1000);

    	});
    }
    
  };

  angular.module('messe.courier.list.turnovers',
    [
  
    ])

    .controller('ListCourierTurnoverController', ListCourierTurnoverController);

})();