(function () {

  var UserService = function ($http, storeService, NODE_1) {
    const restored = storeService.get('node');
    var API_REST = restored ? restored.url : NODE_1.url;

    this.getCouriersList = function () {
      return $http.get(API_REST + '/users/couriers');
    };

    this.getRolesList = function () {
      return $http.get(API_REST + '/users/roles');
    };

    this.getGroupsList = function () {
      return $http.get(API_REST + '/users/groups');
    };

    this.getList = function () {
      return $http.get(API_REST + '/users');
    };

    this.save = function (user) {
      return $http.post(API_REST + '/users', user);
    };

    this.update = function (user) {
      return $http.put(API_REST + '/users/' + user.id, user);
    };

    this.getById = function (id) {
      return $http.get(API_REST + '/users/' + id);
    };

    this.deleteUser = function (id) {
      return $http.delete(API_REST + '/users/' + id);
    };

    this.getMe = function () {
      return $http.get(API_REST + '/users/me');
    }

  };

  angular.module('messe.user')
    .service('userService', UserService);
})();
