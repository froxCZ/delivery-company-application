(function() {

    var StoreService = function($window) {
        
        var service = this;
        service.hashmap = [];

        service.put = function (key, val) {
            service.hashmap[key] = val;
            $window.localStorage.setItem(key, JSON.stringify(val));
        };

        service.get = function (key) {
          if(service.hashmap[key]) {
              return service.hashmap[key];
          }
          return JSON.parse($window.localStorage.getItem(key));
        };

        service.remove = function (key) {
            delete service.hashmap[key];
        };
    };

    angular.module('messe.util', []);

    angular.module('messe.util')
        .service('storeService', StoreService);
})();
