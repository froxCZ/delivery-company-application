(function () {
  'use strict';

  angular.module('messe.websocket', [])
    .service('deliveryService', function (WS_URL_1, WS_URL_2, NODE_1, storeService) {

      var self = this;
      self.listeners = [];
      var WS_URL = storeService.get('node').id === NODE_1.id ? WS_URL_1 : WS_URL_2;
      var me = storeService.get('me') ? storeService.get('me').id : 'none';

      self.onMessage = function (event) {
        if(event.data === "update") {
          self.notifyAll();
        }
      };

      // cmd.val
      self.onOpen = function (event) {
        self.socket.send('open.' + me);
      };

      self.onClose = function (event) {
        self.socket.send('close.' + me);
      };

      self.sendUpdate = function () {
        self.socket.send('update.' + me);
      };

      self.registerCallback = function (callback) {
        self.listeners.push(callback);
      };

      self.notifyAll = function () {
        for (var i = 0; i < self.listeners.length; i++) {
          var listener = self.listeners[i];
          listener();
        }
      };

      self.socket = new WebSocket(WS_URL);
      self.socket.onmessage = self.onMessage;
      self.socket.onopen = self.onOpen;
      self.socket.onclose = self.onClose;

    });

})();