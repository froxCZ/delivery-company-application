(function () {
  "use strict";

  var NavigationController = function (storeService, LOGIN_ID, $location, NODE_1, NODE_2) {
    var nav = this;
    nav.user = storeService.get('me') || null;
    nav.node = storeService.get('node') || null;

    /**
     * Pro roleName vrati, jestli povoleno nebo ne. Admin vidi vse.
     * @param roleName role nebo prazdny (tj. netreba vsude psat admin roli)
     * @returns {boolean} zobraz polozku nebo ne
     */
    nav.showFor = function (roleName) {
      if (roleName === 'all' || nav.isAdmin()) {
        return true;
      }

      if (!roleName) {
        return false;
      }

      var idx = nav.user.roles.indexOf(roleName.toUpperCase());
      return idx > -1;
    };

    nav.isAdmin = function () {
      var idx = nav.user.roles.indexOf('ADMIN');
      return idx > -1;
    };

    nav.login = function (id) {
      storeService.put(LOGIN_ID, id);
      location.reload(true); // reload hard whole page
      $location.path('/');
    };

    nav.active = function (id) {
      return (id === +storeService.get(LOGIN_ID)) ? 'active' : '';
    };

    nav.data = {
      user: nav.user.id + '',
      node: nav.node.id + ''
    };

    nav.userChanged = function () {
      nav.login(nav.data.user);
    };

    nav.nodeChanged = function () {
      storeService.put('node', +nav.data.node === NODE_1.id ? NODE_1 : NODE_2);
    }

  };

  angular.module('messe.navbar',
    [])

    .controller('NavigationController', NavigationController);

})();

