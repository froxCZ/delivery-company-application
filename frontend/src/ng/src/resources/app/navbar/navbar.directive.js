angular.module('messe.navbar')
    .directive('navbar',
        function() {

            return {
                restrict: 'E',
                templateUrl: 'resources/app/navbar/navbar.html',
                controller: 'NavigationController',
                controllerAs: 'nav'
            };
        });