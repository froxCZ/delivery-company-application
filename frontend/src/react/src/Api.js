import React, {Component} from 'react';
import logo from './logo.svg';
import {FieldGroup, ButtonToolbar, Button} from 'react-bootstrap';
import UserForm from "./components/UserForm"
import './App.css';

class Api {
    constructor() {
        this.instance = localStorage.getItem("instance")
        if (this.instance == null) {
            this.instance = "8081";
        }
    }

    fetch(url, myInit) {
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myInit.headers = myHeaders;
        if (typeof myInit.body != 'string' || !myInit.body instanceof String) {
            myInit.body = JSON.stringify(myInit.body)
        }
        let host = "http://localhost:" + this.instance;//todo switch based on settings
        let request = new Request(host + url, myInit);
        return fetch(request).then((response) => {
            if (response.status >= 200 && response.status < 300) {
                if (myInit.toJson === false) {
                    return response;
                } else {
                    return response.json()
                }
            } else {
                let error = new Error(response.status);
                error.status = response.status
                error.json = response.json()
                return Promise.reject(error)
            }
        });
    }

    setInstance(instance) {
        localStorage.setItem("instance", instance);
        this.instance = instance;
    }


}

export default Api = new Api();
