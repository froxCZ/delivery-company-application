import React, {Component} from 'react';
import {FieldGroup, ButtonToolbar, Button} from 'react-bootstrap';
import DatePicker from "react-bootstrap-date-picker"

const DEFAULTS = {
    weight: 1000,
    width: 50,
    height: 50,
    depth: 50
};
class PackagesForm extends Component {
    constructor() {
        super();
        let state = {packages: []};
        this.state = state;

    }

    componentWillMount() {
        this.addPackage();
    }

    handleChange(field, packageObj, e) {
        packageObj[field] = e.target.value;
        this.forceUpdate();
    }

    render() {
        console.log(this.state)
        return (
            <fieldset>
                <div className="row">
                    <div className="col-md-6">
                        {this.state.packages.map(packageObj => this.renderPackage(packageObj))}
                    </div>


                </div>
                <div className="row">
                    <div className="col-md-6 col-md-offset-1">
                        <div className="form-group">

                            <Button bsStyle="primary" style={{"float": "left"}}
                                    onClick={() => this.addPackage()}>Add package</Button>
                            <Button bsStyle="primary" style={{"float": "left"}}
                                    onClick={() => this.removePackage()}>Remove package</Button>
                        </div>

                    </div>
                </div>
            </fieldset>

        );
    }

    renderPackage(packageObj) {
        return <div className="form-group">
            <label className="col-sm-2 control-label">Weight:</label>
            <div className="col-sm-2">
                <input type="text" className="form-control"
                       value={packageObj.weight}
                       onChange={e => this.handleChange("weight", packageObj, e)}
                       id="input-weight"/>
            </div>
            <label className="col-sm-2 control-label">H x W x D:</label>
            <div className="col-sm-2">
                <input type="text" className="form-control" value={packageObj.height}
                       onChange={e => this.handleChange("height", packageObj, e)}/>
            </div>
            <div className="col-sm-2">
                <input type="text" className="form-control" value={packageObj.width}
                       onChange={e => this.handleChange("width", packageObj, e)}/>
            </div>
            <div className="col-sm-2">
                <input type="text" className="form-control" value={packageObj.depth}
                       onChange={e => this.handleChange("depth", packageObj, e)}/>
            </div>
        </div>
    }

    addPackage() {
        let packages = this.state.packages;
        packages.push(Object.assign({}, DEFAULTS))
        this.setState({packages: packages});
    }

    removePackage() {
        this.state.packages.pop();
        this.forceUpdate();
    }

    getPackages(){
        return this.state.packages;
    }
}

export default PackagesForm;
