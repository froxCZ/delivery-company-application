import React, {Component} from 'react';
import {FieldGroup, ButtonToolbar, Button} from 'react-bootstrap';
import DatePicker from "react-bootstrap-date-picker"
const PICKUP_DEFAULTS = {
    name: "Vojta",
    street: "Dvouramenna",
    houseNumber: "8",
    postalcode: "14000",
    city: "Praha",
    dateFrom: "2017-05-01T10:00:00.000Z",
    dateTo: "2017-05-30T10:00:00.000Z"
};
const DELIVERY_DEFAULTS = {
    name: "Petr",
    street: "Baarova",
    houseNumber: "95",
    postalcode: "25225",
    city: "Jinocany",
    dateFrom: "2017-05-01T10:00:00.000Z",
    dateTo: "2017-05-30T10:00:00.000Z"
};
class UserForm extends Component {
    constructor() {
        super();
        let state = {form: {}};
        this.state = state;

    }

    componentWillMount() {
        let defaults
        if (this.props.type == "pickup") {
            defaults = PICKUP_DEFAULTS;
        } else {
            defaults = DELIVERY_DEFAULTS;
        }
        this.setState({form: defaults});
    }

    handleChange(field, e) {
        let newForm = this.state.form;
        newForm[field] = e.target.value;
        this.setState({form: newForm})
    }

    handleDateChange(field, value) {
        let newForm = this.state.form;
        newForm[field] = value
        this.setState({form: newForm})
    }

    render() {
        return (<div>
                <fieldset>
                    <div className="form-group">
                        <label className="col-sm-2">Name:</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control"
                                   defaultValue={this.state.form.name}
                                   onChange={e => this.handleChange("name", e)}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2">Telephone:</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control"
                                   onChange={e => this.handleChange("telephone", e)}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label">Street:</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control"
                                   defaultValue={this.state.form.street}
                                   onChange={e => this.handleChange("street", e)}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2">House number</label>
                        <div className="col-sm-3">
                            <input type="text" className="form-control"
                                   defaultValue={this.state.form.houseNumber}
                                   onChange={e => this.handleChange("houseNumber", e)}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2">Postal Code:</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control"
                                   defaultValue={this.state.form.postalcode}
                                   onChange={e => this.handleChange("postalcode", e)}/>
                        </div>

                    </div>
                    <br/>
                    <div className="form-group">
                        <label className="col-sm-2">City:</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control"
                                   defaultValue={this.state.form.city}
                                   onChange={e => this.handleChange("city", e)}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2">Date from:</label>
                        <div className="col-sm-10">
                            <DatePicker id="example-datepicker"
                                        value={this.state.form.dateFrom}
                                        onChange={e => this.handleDateChange("dateFrom", e)}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2">Date to:</label>
                        <div className="col-sm-10">
                            <DatePicker id="example-datepicker"
                                        value={this.state.form.dateTo}
                                        onChange={e => this.handleDateChange("dateTo", e)}/>
                        </div>
                    </div>
                </fieldset>
            </div>

        );
    }

    getFormData() {
        return this.state.form;
    }
}

export default UserForm;
