import React, {Component} from 'react';
import {FieldGroup, ButtonToolbar, Button} from 'react-bootstrap';
import UserForm from "./UserForm"
import PackagesForm from "./PackagesForm"
import Api from "../Api"


class CreateOrderForm extends Component {
    constructor() {
        super();
        let state = {form: {}};
        state.orderPrice = "?";
        this.state = state;
    }

    componentDidMount() {
        Api.fetch("/rest/products", {method: "GET"}).then(response => {
            console.log(response)
            this.setState({products: response, productId: response[0].id});
        });
        Api.fetch("/rest/users/groups", {method: "GET"}).then(response => {
            this.setState({groups: response, groupId: response[0].id});
        });

    }

    handleChange(field, e) {
        let newForm = this.state.form;
        newForm[field] = e.target.value;
        this.setState({form: newForm})
    }

    render() {
        return (
            <div>

                <div className="row">
                    <div className="col-md-6">
                        <h3>Pickup</h3>
                        <UserForm ref="pickup" type="pickup"/>
                    </div>
                    <div className="col-md-6">
                        <h3>Delivery</h3>
                        <UserForm ref="delivery" type="delivery"/>
                    </div>
                </div>

                <div className="row">
                    <fieldset>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label className="col-sm-2">Product:</label>
                                <div className="col-sm-10">
                                    <select className="form-control"
                                            onChange={e => this.setState({productId: e.target.value})}>
                                        {this.state.products && this.state.products.map(product => <option
                                            value={product.id}>
                                            {product.name}</option>)}


                                    </select>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-sm-2">Group:</label>
                                <div className="col-sm-10">
                                    <select className="form-control"
                                            onChange={e => this.setState({groupId: e.target.value})}>
                                        {this.state.groups && this.state.groups.map(group => <option
                                            value={group.id}>
                                            {group.name}</option>)}


                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label className="col-sm-2">Price:</label>
                                <div className="col-sm-10">
                                    <p style={{
                                        "text-align": "left",
                                        "font-size": "x-large"
                                    }}>{this.state.orderPrice}</p>

                                    <Button bsStyle="primary" style={{"float": "left"}}
                                            onClick={() => this.computePrice()}>Update price</Button>
                                </div>

                            </div>

                        </div>
                    </fieldset>
                </div>
                <PackagesForm ref="packagesForm"/>
                <div className="row">
                    <Button bsStyle="success" style={{"float": "left"}}
                            bsSize="large"
                            onClick={() => this.createOrder()}>Create order</Button>
                </div>

            </div>
        );
    }

    computePrice() {
        let pickup = this.refs.pickup.getFormData();
        let delivery = this.refs.delivery.getFormData();
        let url = "/rest/orders/computePrice?origins="
            + this.createOneLineAddress(pickup) + "&destinations=" + this.createOneLineAddress(delivery)
            + "&productId=" + this.state.productId;
        Api.fetch(url, {method: "GET"}).then(response => {
            this.setState({orderPrice: response})
        });
    }

    createOrder() {
        let body = {
            pickup: this.refs.pickup.getFormData(),
            delivery: this.refs.delivery.getFormData(),
            product: this.state.productId,
            group: this.state.groupId,
            packages: this.refs.packagesForm.getPackages(),
        };
        Api.fetch("/rest/orders", {method: "POST", body: body}).then(response => {
            this.props.orderCreated(response.code);
        });
    }

    createOneLineAddress(point) {
        return point.street + " " + point.houseNumber + " " + point.city;
    }

    generateRandomNumber() {
        return Math.floor(Math.random() * 1000000000000);
    }
}

export default CreateOrderForm;
