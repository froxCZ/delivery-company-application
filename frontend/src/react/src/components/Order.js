import React, {Component} from 'react';
import {FieldGroup, ButtonToolbar, Button} from 'react-bootstrap';
import DatePicker from "react-bootstrap-date-picker"
import JSONPretty from "react-json-pretty"
import 'react-json-pretty/JSONPretty.monikai.styl';
import Api from "../Api"
const preStyle = {
    display: 'block',
    padding: '10px 30px',
    margin: '0',
    overflow: 'scroll',
};
class Order extends Component {
    constructor() {
        super();
        this.state = {};
        this.state.order = null;

    }

    componentWillMount() {
        this.pullInterval = setInterval(this.pull.bind(this), 3000);
    }

    componentWillUnmount() {
        clearInterval(this.pullInterval)
    }

    pull(nextState) {
        Api.fetch("/rest/orders/" + this.props.code + "/qrcode", {method: "GET", toJson: false})
            .then(response => response.blob())
            .then(blob => {
                let urlCreator = window.URL || window.webkitURL;
                let imageUrl = urlCreator.createObjectURL(blob);
                this.setState({qrCodeBlobUrl: imageUrl});
                clearInterval(this.pullInterval)
            });
    }

    render() {
        return (<div>
                <h2>{this.props.code}</h2>
                <h3>QrCode:</h3>
                {this.state.qrCodeBlobUrl ? <img src={this.state.qrCodeBlobUrl}/> : <p>loading..</p>}
                <h3>Order json:</h3>
                {this.state.order ? <pre style={preStyle}>{JSON.stringify(this.state.order, null, 2)}</pre> :
                    <p>loading..</p>}


            </div>

        );
    }
}

export default Order;
