import React, {Component} from 'react';
import logo from './logo.svg';
import {FieldGroup, ButtonToolbar, Button} from 'react-bootstrap';
import CreateOrderForm from "./components/CreateOrderForm"
import Order from "./components/Order"
import './App.css';
import Api from "./Api"

class App extends Component {

    constructor() {
        super();
        this.state = {};
        //this.state.orderCode = "xasd";
        this.timeout = null;
    }

    orderCreated(code) {
        this.setState({orderCode: code});
    }

    instanceChange(instance) {
        Api.setInstance(instance)
        this.forceUpdate();
    }


    render() {
        return (
            <div className="App">
                <div className="header">BE port: <input value={Api.instance} onChange={e => {
                    this.instanceChange(e.target.value)
                }}/> OrderCode: <input value={this.state.orderCode} onChange={e => {
                    let val = e.target.value;
                    this.setState({orderCode: val.length > 0 ? val : null});
                }}/>

                </div>
                <div className="content">
                    {this.state.orderCode == null && <CreateOrderForm orderCreated={this.orderCreated.bind(this)}/>}
                    {this.state.orderCode != null && <Order code={this.state.orderCode}/>}
                </div>
            </div>
        );
    }
}

export default App;
