package com.wa2.qrcodeworker;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class QrCodeApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(QrCodeApplication.class)
                .web(false).run(args);
    }



}
