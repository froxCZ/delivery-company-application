package com.wa2.qrcodeworker;

import java.util.HashMap;
import java.util.Map;

import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RabbitReceiver {

    @Autowired
    private RabbitTemplate rabbitTemplate;


    @Autowired
    QrCodeGeneratorService qrCodeGeneratorService;

    @RabbitListener(queues = RabbitConfig.INCOMING_QUEUE_NAME)
    public void rabbitListener(byte[] bytes,
                               Message message) {
        Exception exception;
        try {
            System.out.println("Starting processing message...");
            this.processMessage(bytes);
            return;
        } catch (Exception e) {
            System.out.println("Failed: " + e.getMessage());
            exception = e;
        }
        Integer attempt = (Integer) message.getMessageProperties()
                .getHeaders().getOrDefault("x-delivery-attempt", 1);

        if (attempt > 5) {
            System.out.println("Rejecting!");
            throw new AmqpRejectAndDontRequeueException("Too many attempts");
        } else {
            Message newMessage = MessageBuilder.fromClonedMessage(message)
                    .setHeader("x-delivery-attempt", ++attempt)
                    .setHeader("x-last-exception", exception.getMessage())
                    .build();
            newMessage.getMessageProperties().setConsumerTag(null);
            rabbitTemplate.send(newMessage.getMessageProperties().getReceivedExchange(),
                    newMessage.getMessageProperties().getReceivedRoutingKey(), newMessage);
        }
    }


    private void processMessage(byte[] bytes) throws Exception {
        String message = new String(bytes, "UTF-8");
        Map<String, String> map = new ObjectMapper().readValue(message, HashMap.class);
        qrCodeGeneratorService.generateOrderQrCode(map);
    }

}