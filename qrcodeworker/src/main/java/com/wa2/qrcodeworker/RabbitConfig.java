package com.wa2.qrcodeworker;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
    final public static String INCOMING_QUEUE_NAME = "newOrders.queue";
    final public static String DEAD_LETTER_QUEUE_NAME = "newOrders.queue.dead-letter";
    final public static String ROUTING_KEY_NAME = "basic";
    final public static String EXCHANGE_NAME = "newOrders.exchange";

    @Bean
    Queue incomingQueue() {
        return QueueBuilder.durable(INCOMING_QUEUE_NAME)
                .withArgument("x-dead-letter-exchange", "")
                .withArgument("x-dead-letter-routing-key", DEAD_LETTER_QUEUE_NAME)
                .build();
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(EXCHANGE_NAME);
    }

    @Bean
    Binding binding() {
        return BindingBuilder.bind(incomingQueue()).to(exchange()).with(ROUTING_KEY_NAME);
    }

    @Bean
    Queue deadLetterQueue() {
        return QueueBuilder.durable(DEAD_LETTER_QUEUE_NAME).build();
    }

    @Bean
    public RabbitReceiver fooListener() {
        return new RabbitReceiver();
    }
}
