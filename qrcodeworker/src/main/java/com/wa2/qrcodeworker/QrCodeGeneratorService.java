package com.wa2.qrcodeworker;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Paths;
import java.util.Hashtable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

@Service
public class QrCodeGeneratorService {
    @Value("${messe.hdfs}")
    private String hdfsPath;

    private File qrCodesDir;

    @PostConstruct
    public void init() {
        qrCodesDir = Paths.get(hdfsPath, "qrcodes").toFile();
        if (!qrCodesDir.exists()) {
            qrCodesDir.mkdirs();
        }
    }

    public void generateOrderQrCode(Map<String, String> jsonMap) throws Exception {
        String code = jsonMap.get("code");
        if (code == null) {
            throw new IllegalStateException("missing qr code!");
        }
        System.out.println("generating qr code for order " + code + " ....");
        File qrFile = new File(qrCodesDir, code + ".png");
        Thread.sleep(15000);
        this.generateQrCode(qrFile, new ObjectMapper().writeValueAsString(jsonMap));
        System.out.println("created qr code for order " + code);
    }

    /**
     * inspired by https://www.javacodegeeks.com/2012/10/generate-qr-code-image-from-java-program.html
     */
    private void generateQrCode(File qrFile, String qrCodeText) throws Exception {
        int size = 500;
        // Create the ByteMatrix for the QR-Code that encodes the given String
        Hashtable hintMap = new Hashtable();
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix byteMatrix = qrCodeWriter.encode(qrCodeText,
                BarcodeFormat.QR_CODE, size, size, hintMap);
        // Make the BufferedImage that are to hold the QRCode
        int matrixWidth = byteMatrix.getWidth();
        BufferedImage image = new BufferedImage(matrixWidth, matrixWidth,
                BufferedImage.TYPE_INT_RGB);
        image.createGraphics();

        Graphics2D graphics = (Graphics2D) image.getGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, matrixWidth, matrixWidth);
        // Paint and save the image using the ByteMatrix
        graphics.setColor(Color.BLACK);

        for (int i = 0; i < matrixWidth; i++) {
            for (int j = 0; j < matrixWidth; j++) {
                if (byteMatrix.get(i, j)) {
                    graphics.fillRect(i, j, 1, 1);
                }
            }
        }
        ImageIO.write(image, "png", qrFile);
    }


}
