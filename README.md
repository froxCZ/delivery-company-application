## Delivery Company system
### About
Application simulating IT system of a delivery company. Used Spring Boot, Hibernate, React, RabbitMQ.

###Features
- create new shipments
- calculate distance and delivery cost via Google Maps API from origin and destination address
- 3 roles: client (creates shipments), admin (manages all users and assigns couriers to shipments) and courier (delivers shipments)
- generating overview of delivered shipments for each courier
- generating QR code by scalable worker instances 

### Implementation 
- `core` module contains REST API for creating and managing shipments
- `frontend` module contains old client in Angular for managing users and shipments and new client in React for creating new shipments.
- `qrcodeworker` is a module for demonstrating scalable background worker instance, which receives new shipments info via RabbitMQ and generates QR codes.
- RabbitMQ messages have 3 attempts to be processed or they are put into a poison queue. 

## Setup

### Modules
 - Use `main/resources/application.properties.template` in each module to set port and other configuration.
 - start each module with `mvn spring-boot:run` 

### Rabbit mq
run docker: `docker run -v <pathToLocalDataDir>:/bitnami/rabbitmq bitnami/rabbitmq:latest`

with `docker ps` and `docker inspect` find the ip

access ui via the ip and port `15672`

Via web ui create queue newOrders and exchange newOrders-exchange. Then set routing from the exchange to newOrders on routing key `basic`.
