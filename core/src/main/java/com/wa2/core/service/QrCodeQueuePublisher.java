package com.wa2.core.service;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wa2.common.model.order.Delivery;

import static com.wa2.core.MesseApplication.EXCHANGE_NAME;

@Service
public class QrCodeQueuePublisher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void enqueue(Delivery delivery) {
        Map<String,String> jsonMap = new HashMap<>();
        jsonMap.put("code",delivery.getCode());
        jsonMap.put("name",delivery.getDeliveryPoint().getName());
        jsonMap.put("telephone",delivery.getDeliveryPoint().getTelephone());
        jsonMap.put("addressLine1",delivery.getDeliveryPoint().getAddress().getAddressLine1());
        jsonMap.put("addressLine2",delivery.getDeliveryPoint().getAddress().getAddressLine2());
        try {
            String json = new ObjectMapper().writeValueAsString(jsonMap);
            rabbitTemplate.convertAndSend(EXCHANGE_NAME, "basic", json.getBytes("UTF-8"));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


}
