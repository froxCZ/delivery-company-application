package com.wa2.core.service;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.wa2.common.model.Member;
import com.wa2.common.model.Role;
import com.wa2.common.model.Turnover;
import com.wa2.common.model.order.Delivery;
import com.wa2.core.data.TurnoverRepository;
import com.wa2.core.turnover_generator.TurnoverGenerator;
import com.wa2.core.turnover_generator.TurnoverJobResult;

@Service
public class TurnoverService {

    @Inject
    private TurnoverRepository turnoverRepository;

    @Inject
    private TurnoverGenerator turnoverGenerator;

    @Inject
    private MemberService memberService;

    public void generateTurnovers(Date fromDate, Date toDate) {
        for (Member courier : memberService.findAllByRole(new Role(Role.COURIER_ROLE))) {
            TurnoverJobResult result = turnoverGenerator.generateForCourier(courier, fromDate, toDate);
            if (result != null) {
                saveTurnoverJobResult(result);
            }
        }
    }

    public List<Turnover> findAllForUser(Member member) {
        return turnoverRepository.findAllOrderedById(member);
    }

    public List<Turnover> findAllOrderedById() {
        return turnoverRepository.findAllOrderedById();
    }

    public void saveTurnoverJobResult(TurnoverJobResult turnoverJobResult) {
        for (Delivery delivery : turnoverJobResult.getDeliveryList()) {
            delivery.setIsAccounted(Boolean.TRUE);
        }
        turnoverRepository.save(turnoverJobResult.getTurnover());
    }
}
