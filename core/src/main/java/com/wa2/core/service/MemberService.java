package com.wa2.core.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.wa2.common.model.Group;
import com.wa2.common.model.Member;
import com.wa2.common.model.Role;
import com.wa2.core.converter.GroupDtoConverter;
import com.wa2.core.data.MemberRepository;

@Service
public class MemberService {
    @Inject
    MemberRepository memberRepository;

    @Inject
    GroupDtoConverter groupDtoConverter;

    public void createMember(Member member) {
        updateMember(member);
    }

    public void updateMember(Member member) {
        if (hasValidRoles(member)) {
            memberRepository.save(member);
        } else {
            throw new IllegalArgumentException("Member has invalid role.");
        }
    }

    private boolean hasValidRoles(Member member) {
        for (Role r : member.getRoles()) {
            if (!Role.isValidRole(r)) {
                return false;
            }
        }
        return true;
    }

    public List<Member> findAllByRole(Role role) {
        return memberRepository.findAllByRole(role);
    }

    public List<Member> findAllOrderedByName() {
        return memberRepository.findAllOrderedByName();
    }

    public Member findById(Long id) {
        return memberRepository.findById(id);
    }

    public List<String> getAllRoles() {
        return memberRepository.getAllRoles();
    }

    public Member findByEmail(String email) {
        return memberRepository.findByEmail(email);
    }

    public List<Group> getAllGroups() {
        return memberRepository.findGroupsOrderedByName();
    }

    public List<Group> findGroups() {
        return memberRepository.findGroupsOrderedByName();
    }
}

