package com.wa2.core.service;

import java.io.IOException;
import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wa2.common.model.product.Product;

@Service
public class PriceService {

    private static final String apikey = "AIzaSyBGbCu8nYsn1kKvv3HF9JTVDp112Tuz-tU";
    public static final String URL = "https://maps.googleapis.com/maps/api/distancematrix/json?";

    public BigDecimal compute(String origins, String destinations, Product product) {
        if (product == null) {
            return null;
        }
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL)
                // Add query parameter
                .queryParam("key", apikey)
                .queryParam("origins", origins)
                .queryParam("destinations", destinations);

        String response = new RestTemplate().getForObject(builder.build().toUri(), String.class);
        System.out.println(response);

        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = null;
            root = mapper.readTree(response);
            String distance = root.get("rows").get(0).get("elements").get(0).get("distance").get("value").asText();
            BigDecimal b = new BigDecimal(distance);
            return b.divide(new BigDecimal(1000)).multiply(product.getPricePerKm()).add(product.getBasePrice());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BigDecimal.valueOf(10);
    }
}
