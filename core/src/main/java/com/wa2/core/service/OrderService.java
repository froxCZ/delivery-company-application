package com.wa2.core.service;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.wa2.common.model.Member;
import com.wa2.common.model.order.Delivery;
import com.wa2.common.model.order.OrderState;
import com.wa2.core.data.product.OrderRepository;

@Service
public class OrderService {
    @Value("${messe.hdfs}")
    private String hdfsPath;

    @Inject
    OrderRepository orderRepository;

    @Inject
    PriceService priceService;

    @Inject
    QrCodeQueuePublisher qrCodeQueuePublisher;


    private File qrCodesDir;

    @PostConstruct
    public void init() {
        qrCodesDir = Paths.get(hdfsPath, "qrcodes").toFile();
        if (!qrCodesDir.exists()) {
            qrCodesDir.mkdirs();
        }
    }

    public String createOrder(Delivery delivery) {
        String code = UUID.randomUUID().toString();
        delivery.setCode(code);
        if (delivery.getCourier() == null) {
            delivery.setState(OrderState.CREATED);
        } else {
            delivery.setState(OrderState.ASSIGNED);
        }
        delivery.setPrice(priceService.compute(delivery.getPickupPoint().getAddress().getAddressLine1(),
                delivery.getDeliveryPoint().getAddress().getAddressLine1(), delivery.getProduct()));
        orderRepository.save(delivery);
        qrCodeQueuePublisher.enqueue(delivery);
        return code;
    }

    public Delivery findOrder(Long id) {
        return orderRepository.findById(id);
    }

    public Delivery findOrderByCode(String code) {
        return orderRepository.findByCode(code);
    }

    public void updateOrder(Delivery delivery) {
        orderRepository.save(delivery);
    }

    public List<Delivery> findAllOrderedById() {
        return orderRepository.findAllOrderedById();
    }

    public List<Delivery> findUndeliveredOrders(Member loggedInMember) {
        return orderRepository.findUndeliveredOrders(loggedInMember);
    }

    public byte[] getQrCode(String code) throws Exception {
        File qrFile = new File(qrCodesDir, code + ".png");
        return IOUtils.toByteArray(new FileInputStream(qrFile));
    }

    public List<Delivery> findUndeliveredOrdersGroup(Member loggedInMember) {
        return orderRepository.findUndeliveredOrdersWithinGroup(loggedInMember.getGroups());
    }

}

