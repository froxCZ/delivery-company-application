package com.wa2.core.dto.order;

import com.wa2.core.dto.GroupDTO;

import java.util.List;

public class OrderDTO {

	private String id;

	private String state;

	private String code;

	private String price;
	
	private String product;
	
	private String courier;
	
	private String productName;

	private String stateName;

	private String courierName;

	private String groupName;

	private String group;

	private List<PackageDTO> packages;
	
	private OrderPointDTO pickup;
	
	private OrderPointDTO delivery;
	
	public OrderDTO() {
	}
	
	public OrderDTO(String id, String state, String code, String price, String product, String courier,
			List<PackageDTO> packages, OrderPointDTO pickup, OrderPointDTO delivery, String productName,
					String courierName, String group, String groupName) {
		super();
		this.id = id;
		this.state = state;
		this.code = code;
		this.price = price;
		this.product = product;
		this.courier = courier;
		this.group = group;
		this.packages = packages;
		this.pickup = pickup;
		this.delivery = delivery;
		this.courierName = courierName;
		this.productName = productName;
		this.groupName = groupName;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public List<PackageDTO> getPackages() {
		return packages;
	}
	public void setPackages(List<PackageDTO> packages) {
		this.packages = packages;
	}

	public OrderPointDTO getPickup() {
		return pickup;
	}

	public void setPickup(OrderPointDTO pickup) {
		this.pickup = pickup;
	}

	public OrderPointDTO getDelivery() {
		return delivery;
	}

	public void setDelivery(OrderPointDTO delivery) {
		this.delivery = delivery;
	}

	public String getCourier() {
		return courier;
	}

	public void setCourier(String courier) {
		this.courier = courier;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCourierName() {
		return courierName;
	}

	public void setCourierName(String courierName) {
		this.courierName = courierName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}
