/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wa2.core.data.product;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import com.wa2.common.model.Group;
import org.springframework.stereotype.Repository;

import com.wa2.common.model.Member;
import com.wa2.common.model.order.Delivery;

@Repository
@Transactional
public class OrderRepository {

    @Inject
    private EntityManager em;

    public Delivery findById(Long id) {
        return em.find(Delivery.class, id);
    }

    public void persist(Delivery order) {
        em.persist(order);
    }

    public List<Delivery> findAllOrderedById() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Delivery> criteria = cb.createQuery(Delivery.class);
        Root<Delivery> order = criteria.from(Delivery.class);
        criteria.select(order).orderBy(cb.asc(order.get("id")));
        return em.createQuery(criteria).getResultList();
    }

    public List<Delivery> findDeliveredNotAccountedOrdersOfMessengerInPeriod(Member messenger, Date fromDate, Date toDate) {
        return em.createNamedQuery("Delivery.findDeliveredNotAccountedOrdersOfMessengerInPeriod", Delivery.class).setParameter("courier", messenger)
                .setParameter("fromDate", fromDate).setParameter("toDate", toDate).getResultList();
    }

    public List<Delivery> findCompletedOrdersInPeriod(Date fromDate, Date toDate) {
        return em.createNamedQuery("Delivery.findCompletedOrdersInPeriod", Delivery.class)
                .setParameter("fromDate", fromDate).setParameter("toDate", toDate).getResultList();
    }

    public void save(Delivery delivery) {
        em.merge(delivery);
    }

    public List<Delivery> findUndeliveredOrders(Member courier) {
        return em.createQuery("SELECT d FROM Delivery d WHERE d.courier = :courier " +
                "AND d.state<>com.wa2.common.model.order.OrderState.DELIVERED").setParameter("courier", courier).getResultList();
    }

    public Delivery findByCode(String code) {
        return em.createNamedQuery("Delivery.findByCode", Delivery.class)
                .setParameter("code", code).getSingleResult();
    }

    public List<Delivery> findUndeliveredOrdersWithinGroup(Set<Group> groups) {
        Query query = em.createQuery("SELECT d FROM Delivery d WHERE d.group IN (:groups) " +
                "AND d.state<>com.wa2.common.model.order.OrderState.DELIVERED");
        query.setParameter("groups", groups);
        return query.getResultList();
    }
}
