package com.wa2.core.converter;

import com.wa2.common.model.Group;
import com.wa2.common.model.product.Product;
import com.wa2.core.dto.GroupDTO;
import com.wa2.core.dto.order.ProductDTO;
import org.springframework.stereotype.Component;

@Component
public class GroupDtoConverter extends SuperDtoConverter<Group, GroupDTO> {

	@Override
	public GroupDTO convert(Group g) {
		if(g == null){
			return null;
		}
		return new GroupDTO(g.getId() + "", g.getName());
	}

	@Override
	public Group convertDto(GroupDTO input) {
		return null;
	}

}
