package com.wa2.core.converter;

import org.springframework.stereotype.Component;

import com.wa2.core.dto.order.ProductDTO;
import com.wa2.common.model.product.Product;

@Component
public class ProductDtoConverter extends SuperDtoConverter<Product, ProductDTO> {

	@Override
	public ProductDTO convert(Product p) {	
		if(p == null){
			return null;
		}
		return new ProductDTO(p.getId().toString(), p.getName(), p.getPricePerKm().toPlainString(), p.getBasePrice().toPlainString());
	}

	@Override
	public Product convertDto(ProductDTO input) {
		return null;
	}

}
