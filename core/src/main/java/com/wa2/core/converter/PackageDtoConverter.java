package com.wa2.core.converter;

import org.springframework.stereotype.Component;

import com.wa2.core.dto.order.PackageDTO;

@Component
public class PackageDtoConverter extends SuperDtoConverter<com.wa2.common.model.order.Package, PackageDTO> {

	@Override
	public PackageDTO convert(com.wa2.common.model.order.Package p) {
		if(p == null){
			return null;
		}
		return new PackageDTO(
				p.getId().toString(), 
				p.getWeight().toString(), 
				p.getHeight().toString(), 
				p.getWidth().toString(), 
				p.getDepth().toString());
	}

	@Override
	public com.wa2.common.model.order.Package convertDto(PackageDTO input) {
		return null;
	}

}
