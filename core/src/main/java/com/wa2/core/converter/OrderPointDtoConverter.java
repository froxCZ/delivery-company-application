package com.wa2.core.converter;

import org.springframework.stereotype.Component;

import com.wa2.core.dto.order.OrderPointDTO;
import com.wa2.common.model.order.OrderPoint;

@Component
public class OrderPointDtoConverter extends SuperDtoConverter<OrderPoint, OrderPointDTO> {
	
	@Override
	public OrderPointDTO convert(OrderPoint o) {	
		if(o == null){
			return null;
		}
		return new OrderPointDTO(
			o.getName(), 
			o.getTelephone(), 
			o.getNote(), 
			o.getAddress().getStreet(),
			o.getAddress().getPostalCode(), 
			o.getAddress().getHouseNumber(),
			o.getAddress().getCity(), 
			o.getDateFrom().toString(),
			o.getDateTo().toString());
	}

	@Override
	public OrderPoint convertDto(OrderPointDTO input) {
		return null;
	}
}
