package com.wa2.core.converter;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import com.wa2.common.model.Group;
import com.wa2.common.model.Member;
import org.springframework.stereotype.Component;

import com.wa2.core.data.MemberRepository;
import com.wa2.core.dto.order.OrderDTO;
import com.wa2.core.dto.order.PackageDTO;
import com.wa2.core.data.product.ProductRepository;
import com.wa2.core.dto.order.OrderPointDTO;
import com.wa2.common.model.Address;
import com.wa2.common.model.order.Delivery;
import com.wa2.common.model.order.OrderPoint;
import com.wa2.common.model.order.OrderState;

@Component
public class OrderDtoConverter extends SuperDtoConverter<Delivery, OrderDTO> {
    private static final DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

    @Inject
    OrderPointDtoConverter orderPointDtoConverter;
    @Inject
    PackageDtoConverter packagePointDtoConverter;

    @Inject
    private ProductRepository productRepository;
    @Inject
    private MemberRepository memberRepository;

    @Inject
    private GroupDtoConverter groupDtoConverter;

    private Random randomGenerator = new Random();

    @Override
    public OrderDTO convert(Delivery o) {
        if (o == null) {
            return null;
        }
        return new OrderDTO(
                o.getId().toString(),
                o.getState().toString(),
                o.getCode(),
                o.getPrice().toPlainString(),
                o.getProduct().getId().toString(),
                o.getCourier().getId().toString(),
                packagePointDtoConverter.convertToList(o.getPackages()),
                orderPointDtoConverter.convert(o.getPickupPoint()),
                orderPointDtoConverter.convert(o.getDeliveryPoint()),
                o.getProduct().getName(),
                o.getCourier().getName(),
                o.getGroup().getId() + "",
                o.getGroup().getName());
    }

    @Override
    public Delivery convertDto(OrderDTO order) throws ParseException {
        Delivery or = new Delivery();

        or.setPickupPoint(convertOrderPoint(order.getPickup()));
        or.setDeliveryPoint(convertOrderPoint(order.getDelivery()));

        if (order.getPrice() != null) {
            or.setPrice(BigDecimal.valueOf(Double.valueOf(order.getPrice())));
        }
        or.setState(OrderState.fromString(order.getState()));
        or.setProduct(productRepository.findById(Long.parseLong(order.getProduct())));

        Group group = memberRepository.findGroupById(Integer.parseInt(order.getGroup()));

        or.setCourier(getRandomCourierFromGroup(group));
        or.setGroup(group);
        or.setPackages(convertPackages(order.getPackages()));
        return or;
    }

    private OrderPoint convertOrderPoint(OrderPointDTO orderPoint) throws ParseException {

        OrderPoint op = new OrderPoint();

        op.setDateFrom(format.parse(orderPoint.getDateFrom()));
        op.setDateTo(format.parse(orderPoint.getDateTo()));
        op.setName(orderPoint.getName());
        op.setTelephone(orderPoint.getTelephone());
        op.setNote(orderPoint.getNote());
        op.setAddress(convertAddress(orderPoint));

        return op;
    }

    private Address convertAddress(OrderPointDTO orderPoint) throws ParseException {

        Address addr = new Address();

        addr.setStreet(orderPoint.getStreet());
        addr.setHouseNumber(orderPoint.getHouseNumber());
        addr.setPostalCode(orderPoint.getPostalcode());
        addr.setCity(orderPoint.getCity());

        return addr;
    }

    private List<com.wa2.common.model.order.Package> convertPackages(List<PackageDTO> packages) throws ParseException {
        List<com.wa2.common.model.order.Package> pac = new ArrayList<>();

        for (PackageDTO dto : packages) {
            com.wa2.common.model.order.Package p = new com.wa2.common.model.order.Package();
            p.setWeight(Integer.parseInt(dto.getWeight()));
            p.setHeight(Integer.parseInt(dto.getHeight()));
            p.setWidth(Integer.parseInt(dto.getWidth()));
            p.setDepth(Integer.parseInt(dto.getDepth()));
            pac.add(p);
        }
        return pac;
    }

    private Member getRandomCourierFromGroup(Group group) throws ParseException {
        List<Member> couriersInGroup = new ArrayList<>(group.getMembers());
        if (couriersInGroup.isEmpty()) {
            throw new ParseException("empty group, select another one", 0);
        }

        int idx = randomGenerator.nextInt(couriersInGroup.size());
        return memberRepository.findById(couriersInGroup.get(idx).getId());
    }

}