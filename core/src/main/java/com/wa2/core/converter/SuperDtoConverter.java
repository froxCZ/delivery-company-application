package com.wa2.core.converter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public abstract class SuperDtoConverter<Clazz, Dto> {
    
	public abstract Dto convert(Clazz input);

	public Clazz convertDto(Dto input) throws ParseException {
		throw new NotImplementedException();
	}
	
	public List<Dto> convertToList(final List<Clazz> input) {
		List<Dto> converted = new ArrayList<>();
		
		if(input == null || input.size() == 0){
			return converted;
		}
		
		for(Clazz clazz : input){
			converted.add(convert(clazz));
		}
		
        return converted;
    }
}