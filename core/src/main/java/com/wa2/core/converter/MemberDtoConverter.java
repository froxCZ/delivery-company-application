package com.wa2.core.converter;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.wa2.common.model.Group;
import com.wa2.common.model.order.Delivery;
import com.wa2.core.data.MemberRepository;
import com.wa2.core.data.product.OrderRepository;
import com.wa2.core.dto.GroupDTO;
import org.springframework.stereotype.Component;

import com.wa2.core.dto.MemberDTO;
import com.wa2.common.model.Member;
import com.wa2.common.model.Role;
import com.wa2.core.security.DigestPassword;

import javax.inject.Inject;

@Component
public class MemberDtoConverter extends SuperDtoConverter<Member, MemberDTO> {

    @Inject
    MemberRepository memberRepository;

    @Inject
    private OrderRepository orderRepository;

    public MemberDTO convert(Member m) {
        if (m == null) {
            return null;
        }
        MemberDTO convertToDto = new MemberDTO();
        convertToDto.setId(m.getId().toString());
        convertToDto.setName(m.getName());
        convertToDto.setEmail(m.getEmail());
        convertToDto.setPhoneNumber(m.getPhoneNumber());
        if (m.getFee() != null) {
            convertToDto.setFee(m.getFee().toString());
        }
        Set<String> roles = new HashSet<>();
        for (Role r : m.getRoles()) {
            roles.add(r.getRole());
        }
        convertToDto.setRoles(roles);

        Set<GroupDTO> groups = new HashSet<>();
        for (Group g : m.getGroups()) {
            groups.add(new GroupDTO(g.getId() + "", g.getName()));
        }
        convertToDto.setGroups(groups);

        return convertToDto;
    }

    public Member convertDto(MemberDTO memberDTO, Member convertTo) throws ParseException {
        if (memberDTO.getId() != null) {
            convertTo.setId(Long.valueOf(memberDTO.getId()));
        }
        convertTo.setEmail(memberDTO.getEmail());
        if (memberDTO.getPassword() != null) {
            convertTo.setPassword(DigestPassword.hash(memberDTO.getPassword()));
        }
        convertTo.setPhoneNumber(memberDTO.getPhoneNumber());
        convertTo.setName(memberDTO.getName());
        if (memberDTO.getFee() != null) {
            convertTo.setFee(BigDecimal.valueOf(Double.valueOf(memberDTO.getFee())));
        }
        Set<Role> roles = new HashSet<>();
        for (String s : memberDTO.getRoles()) {
            roles.add(new Role(s));
        }
        convertTo.setRoles(roles);

        if (!isSafeUpdateGroups(convertTo, memberDTO)) {
            throw new ParseException("cannot edit groups, undelivered item", 0);
        }

        Set<Group> groups = new HashSet<>();
        List<Group> allGroups = memberRepository.findGroupsOrderedByName();
        Set<Group> groupsToUpdate = new HashSet<>();

        for (Group group : allGroups) {
            if (memberDTO.getGroups().isEmpty()) {
                group.getMembers().remove(convertTo);
                groupsToUpdate.add(group);
                continue;
            }
            for (GroupDTO groupToAdd : memberDTO.getGroups()) {
                int groupToAddId = Integer.parseInt(groupToAdd.getId());
                if (group.getId() == groupToAddId) {
                    // pridani skupiny a usera do ni
                    Group ng = new Group();
                    ng.setName(groupToAdd.getName());
                    ng.setId(groupToAddId);
                    Set<Member> members = group.getMembers();
                    members.add(convertTo);
                    ng.setMembers(members);
                    groups.add(ng);
                } else {
                    // odebirani usera ze skupiny
                    if (groups.contains(group)) continue;

                    Set<Member> members = group.getMembers();
                    if (members.remove(convertTo)) {
                        groupsToUpdate.add(group);
                    }
                }
            }
        }
        updateGroups(groupsToUpdate);

        convertTo.setGroups(groups);

        return convertTo;
    }

    private boolean isSafeUpdateGroups(Member oldMember, MemberDTO newMember) {
        List<Delivery> undeliveredOrders = orderRepository.findUndeliveredOrders(oldMember);

        // projedu nedorucene zasilky kuryra pres jeho nove groupy, jestli se neodebrala groupa nejake zasilky
        int match = 0;
        for (Delivery delivery : undeliveredOrders) {
            String deliveryGroupId = delivery.getGroup().getId() + "";
            for (GroupDTO group : newMember.getGroups()) {
                if(deliveryGroupId.equals(group.getId())) {
                    match++;
                }
            }
        }

        return match == undeliveredOrders.size();
    }

    private void updateGroups(Set<Group> groupsToUpdate) {
        for (Group g : groupsToUpdate) {
            memberRepository.saveGroup(g);
        }
    }


}
