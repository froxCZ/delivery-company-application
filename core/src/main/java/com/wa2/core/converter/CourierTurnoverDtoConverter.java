package com.wa2.core.converter;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.wa2.core.dto.turnover.CourierTurnoverDTO;
import com.wa2.common.util.Util;
import com.wa2.common.model.Turnover;

@Component
public class CourierTurnoverDtoConverter extends SuperDtoConverter<Turnover, CourierTurnoverDTO> {

	@Inject
    Util util;

	@Override
	public CourierTurnoverDTO convert(Turnover s) {
		if(s == null){
			return null;
		}
		return new CourierTurnoverDTO(
				s.getId().toString(),
				util.dateToDateTimeString(s.getCreated()),
				s.getCourier().getName(),
				s.getProfit().toPlainString(),
				s.getMessage(),
				s.getConsignmentSize().toString());
	}
}
