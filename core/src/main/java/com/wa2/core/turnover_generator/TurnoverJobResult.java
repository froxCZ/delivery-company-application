package com.wa2.core.turnover_generator;

import java.util.List;

import com.wa2.common.model.Turnover;
import com.wa2.common.model.order.Delivery;


public class TurnoverJobResult {
    private Turnover turnover;
    private  List<Delivery> deliveryList;

    public TurnoverJobResult(Turnover turnover, List<Delivery> deliveryList) {
        this.turnover = turnover;
        this.deliveryList = deliveryList;
    }

    public Turnover getTurnover() {
        return turnover;
    }

    public List<Delivery> getDeliveryList() {
        return deliveryList;
    }
}
