package com.wa2.core.turnover_generator;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.wa2.common.model.Address;
import com.wa2.common.model.Member;
import com.wa2.common.model.Turnover;
import com.wa2.common.model.order.Delivery;
import com.wa2.core.data.product.OrderRepository;


@Component
public class TurnoverGenerator {

    @Inject
    private OrderRepository orderRepository;

    public TurnoverJobResult generateForCourier(Member courier, Date fromDate, Date toDate) {
        List<Delivery> completedDeliveries = orderRepository.findDeliveredNotAccountedOrdersOfMessengerInPeriod
                (courier, fromDate, toDate);
        if (completedDeliveries.isEmpty()) {//do not create turnover summary if there are no deliveries
            return null;
        }
        BigDecimal deliveriesPriceSum = BigDecimal.ZERO;
        StringBuilder sb = new StringBuilder();
        sb.append("Summary of deliveries from date " + fromDate + " to date " + toDate + " \n\n");
        sb.append("Name: " + courier.getName() + "\n");
        sb.append("Email: " + courier.getEmail() + "\n");
        sb.append("\n\n");
        sb.append("Delivered packets:\n");
        for (Delivery delivery : completedDeliveries) {
            deliveriesPriceSum = deliveriesPriceSum.add(delivery.getPrice());
            Address pickupAddress = delivery.getPickupPoint().getAddress();
            Address deliveryAddress = delivery.getDeliveryPoint().getAddress();
            sb.append(delivery.getId() + "\t" + delivery.getProduct().getName() + "\t" + pickupAddress
                    .getAddressLine1() + "," + pickupAddress.getCity() + "\t" +
                    deliveryAddress.getAddressLine1() + "," + deliveryAddress.getCity() + "\t" + delivery.getPrice()
                    + "\n");
        }
        sb.append("\n");
        sb.append("Total price: " + deliveriesPriceSum + "\n");
        BigDecimal profit = deliveriesPriceSum.multiply(courier.getFee());
        sb.append("Earning: " + profit);
        Turnover turnover = new Turnover();
        turnover.setProfit(profit);
        turnover.setConsignmentSize(completedDeliveries.size());
        turnover.setCreated(new Date());
        turnover.setCourier(courier);
        turnover.setMessage(sb.toString());
        return new TurnoverJobResult(turnover, completedDeliveries);
    }
}
