package com.wa2.core.util;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.ServletWebRequest;

import com.wa2.common.model.Member;
import com.wa2.core.service.MemberService;


/**
 * Must use proxyMode, since it is being injected into singletons.
 * http://stackoverflow.com/questions/34090750/inject-request-scoped-bean-into-another-bean
 */
@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RequestContext {
    @Autowired
    MemberService memberService;

    @Autowired(required = true)
    private ServletWebRequest req;

    Member loggedInMember;


    public Member getLoggedInMember() {
        if (loggedInMember == null) {
            try {
                loggedInMember = memberService.findById(getLoginId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return loggedInMember;
    }

    private Long getLoginId() {
        return Long.valueOf(req.getHeader("X-LOGIN-ID"));
    }

}