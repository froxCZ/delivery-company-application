/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wa2.core.rest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.Validator;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wa2.common.model.Member;
import com.wa2.common.model.Role;
import com.wa2.core.converter.GroupDtoConverter;
import com.wa2.core.converter.MemberDtoConverter;
import com.wa2.core.dto.GroupDTO;
import com.wa2.core.dto.MemberDTO;
import com.wa2.core.service.MemberService;
import com.wa2.core.util.RequestContext;

@RestController
@RequestMapping("/rest/users")
public class UserResourceRESTService {

    @Inject
    private MemberService memberService;

    @Inject
    private MemberDtoConverter memberDtoConverter;

    @Inject
    private GroupDtoConverter groupDtoConverter;

    @Inject
    RequestContext requestContext;

    @Inject
    private Validator validator;

    @RequestMapping(method = RequestMethod.GET, path = "/me")
    public ResponseEntity getLoggedInUser() {
        if (requestContext.getLoggedInMember() == null) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        } else {
            return new ResponseEntity<MemberDTO>(
                    memberDtoConverter.convert(requestContext.getLoggedInMember()), HttpStatus.OK);
        }

    }

    @RequestMapping(method = RequestMethod.GET)
    public List<MemberDTO> listAllUsers() {
        List<Member> users = memberService.findAllOrderedByName();
        return memberDtoConverter.convertToList(users);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    public MemberDTO getUserById(@PathVariable("id") Long id) {
        return memberDtoConverter.convert(memberService.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createMember(@RequestBody MemberDTO memberDTO) {
        System.out.println(memberDTO);
        try {
            Member m = memberDtoConverter.convertDto(memberDTO, new Member());
            validateMember(m);
            memberService.createMember(m);
            return new ResponseEntity(HttpStatus.OK);
        } catch (ConstraintViolationException ce) {
            return createViolationResponse(ce.getConstraintViolations());
        } catch (Exception e) {
            // handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            return new ResponseEntity(responseObj, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{id:[0-9][0-9]*}")
    public ResponseEntity updateMember(@PathVariable("id") Long id,@RequestBody MemberDTO memberDTO) {
        if (!id.toString().equals(memberDTO.getId())) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        try {
            Member m = memberDtoConverter.convertDto(memberDTO, memberService.findById(id));
            validateMember(m);
            memberService.updateMember(m);
            return new ResponseEntity(HttpStatus.OK);
        } catch (ConstraintViolationException ce) {
            return createViolationResponse(ce.getConstraintViolations());
        } catch (Exception e) {
            // handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            return new ResponseEntity(responseObj, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "/admin")
    public List<MemberDTO> getAllAdmins() {
        List<Member> admins = memberService.findAllByRole(new Role(Role.ADMIN_ROLE));
        return memberDtoConverter.convertToList(admins);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/operators")
    public List<MemberDTO> getAllOperators() {
        List<Member> operators = memberService.findAllByRole(new Role(Role.OPERATOR_ROLE));
        return memberDtoConverter.convertToList(operators);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/couriers")
    public List<MemberDTO> listAllCouriers() {
        List<Member> couriers = memberService.findAllByRole(new Role(Role.COURIER_ROLE));
        return memberDtoConverter.convertToList(couriers);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/roles")
    public List<String> listAllRoles() {
        return memberService.getAllRoles();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/groups")
    public List<GroupDTO> listAllGroups() {
        return groupDtoConverter.convertToList(memberService.getAllGroups());
    }

    private void validateMember(Member member) throws ConstraintViolationException, ValidationException {
        Set<ConstraintViolation<Member>> violations = validator.validate(member);

        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(violations));
        }
    }

    private ResponseEntity createViolationResponse(Set<ConstraintViolation<?>> violations) {
        Map<String, String> responseObj = new HashMap<>();

        for (ConstraintViolation<?> violation : violations) {
            responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
        }
        return new ResponseEntity(responseObj, HttpStatus.BAD_REQUEST);
    }
}
