/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wa2.core.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wa2.common.util.Util;
import com.wa2.core.converter.CourierTurnoverDtoConverter;
import com.wa2.core.dto.turnover.CourierTurnoverDTO;
import com.wa2.core.service.TurnoverService;
import com.wa2.core.util.RequestContext;
import com.wa2.core.service.MemberService;

@RestController
@RequestMapping("/rest/turnovers")
public class TurnoverResourceRESTService {

    @Inject
    CourierTurnoverDtoConverter courierTurnoverDtoConverter;

    @Inject
    TurnoverService turnoverService;

    @Inject
    MemberService memberService;

    @Inject
    RequestContext requestContext;

    @Inject
    Util util;

    @RequestMapping(method = RequestMethod.GET, path = "/my")
    public ResponseEntity myTurnovers() {
        if (requestContext.getLoggedInMember() == null) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        } else {
            return new ResponseEntity<List<CourierTurnoverDTO>>
                    (courierTurnoverDtoConverter.convertToList(
                            turnoverService.findAllForUser(requestContext.getLoggedInMember())),
                            HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<CourierTurnoverDTO> listAllTurnovers() {
        return courierTurnoverDtoConverter.convertToList(turnoverService.findAllOrderedById());
    }

    @RequestMapping(method = RequestMethod.POST, path = "/generate")
    public ResponseEntity generateTurnovers(@RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate) {
        turnoverService.generateTurnovers(util.stringToDate(fromDate), util.stringToDate(toDate));
        return new ResponseEntity(HttpStatus.OK);
    }

}
