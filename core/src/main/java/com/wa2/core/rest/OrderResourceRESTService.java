/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wa2.core.rest;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

import javax.inject.Inject;

import com.wa2.common.model.Group;
import com.wa2.core.converter.GroupDtoConverter;
import com.wa2.core.dto.GroupDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wa2.common.model.order.Delivery;
import com.wa2.common.model.order.OrderState;
import com.wa2.core.converter.OrderDtoConverter;
import com.wa2.core.data.product.ProductRepository;
import com.wa2.core.dto.order.OrderDTO;
import com.wa2.core.service.OrderService;
import com.wa2.core.service.PriceService;
import com.wa2.core.util.RequestContext;

@RestController
@RequestMapping("/rest/orders")
public class OrderResourceRESTService {

    @Inject
    OrderDtoConverter orderDtoConverter;

    @Inject
    GroupDtoConverter groupDtoConverter;

    @Inject
    OrderService orderService;

    @Inject
    PriceService priceService;

    @Inject
    RequestContext requestContext;

    @Inject
    private ProductRepository productRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<OrderDTO> listAllOrders() {
        return orderDtoConverter.convertToList(orderService.findAllOrderedById());
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createOrder(@RequestBody OrderDTO orderDto) {
        try {
            String code = orderService.createOrder(orderDtoConverter.convertDto(orderDto));

            return new ResponseEntity(Collections.singletonMap("code", code), HttpStatus.OK);
        } catch (ParseException e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{code}")
    public ResponseEntity getOrder(@PathVariable("code") String code) {
        Delivery delivery = orderService.findOrderByCode(code);
        if (delivery == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(orderDtoConverter.convert(delivery), HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{code}/qrcode", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity getOrderQrCode(@PathVariable("code") String code) {
        try {
            byte[] qrCode = orderService.getQrCode(code);
            return new ResponseEntity(qrCode, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(method = RequestMethod.POST, path = "/{id:[0-9][0-9]*}/startDelivering")
    public ResponseEntity startDelivering(@PathVariable("id") Long id) {
        HttpStatus status = null;
        Delivery delivery = orderService.findOrder(id);
        if (delivery == null) {
            status = HttpStatus.NOT_FOUND;
        } else if (delivery.getState() != OrderState.ASSIGNED) {
            status = HttpStatus.NOT_ACCEPTABLE;
        } else {
            delivery.setState(OrderState.DELIVERING);
            orderService.updateOrder(delivery);
            status = status = HttpStatus.OK;
        }
        return new ResponseEntity(status);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/{id:[0-9][0-9]*}/wasDelivered")
    public ResponseEntity delivered(@PathVariable("id") Long id) {
        Delivery delivery = orderService.findOrder(id);
        HttpStatus status = null;
        if (delivery == null) {
            status = HttpStatus.NOT_FOUND;
        } else if (delivery.getState() != OrderState.DELIVERING) {
            status = HttpStatus.NOT_ACCEPTABLE;
        } else {
            delivery.setState(OrderState.DELIVERED);
            orderService.updateOrder(delivery);
            status = HttpStatus.OK;
        }
        return new ResponseEntity(status);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/mine/undelivered")
    public List<OrderDTO> mineUndeliveredOrders() {
        List<Delivery> deliveries = orderService.findUndeliveredOrdersGroup(requestContext.getLoggedInMember());
        return orderDtoConverter.convertToList(deliveries);
    }

    @RequestMapping(method = RequestMethod.GET, path = "computePrice")
    public BigDecimal computePrice(@RequestParam("origins") String origins, @RequestParam("destinations")
            String destinations, @RequestParam("productId") String productId) {
        return priceService.compute(origins, destinations, productRepository.findById(Long.parseLong(productId)));
    }


}
