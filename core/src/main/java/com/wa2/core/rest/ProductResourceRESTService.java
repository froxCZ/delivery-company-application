/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wa2.core.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wa2.core.converter.ProductDtoConverter;
import com.wa2.core.data.product.ProductRepository;
import com.wa2.core.dto.order.ProductDTO;
import com.wa2.common.model.product.Product;

@RestController
@RequestMapping("/rest/products")
public class ProductResourceRESTService {

    @Inject
    private ProductRepository repository;

    @Inject
    private ProductDtoConverter productConverter;

    @RequestMapping(method = RequestMethod.GET)
    public List<ProductDTO> listAllProducts() {     
    	List<Product> products = repository.findAllOrderedById();
   
    	return productConverter.convertToList(products);
    }


}
