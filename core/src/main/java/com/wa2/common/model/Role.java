package com.wa2.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
//@Table(name = "user_roles", catalog = "test",
//        uniqueConstraints = @UniqueConstraint(
//                columnNames = { "role", "member_id" }))
@Table(name = "user_roles",
        uniqueConstraints = @UniqueConstraint(
                columnNames = { "role", "member_id" }))
public class Role{
    public static final String ADMIN_ROLE = "ADMIN";
    public static final String OPERATOR_ROLE = "OPERATOR";
    public static final String COURIER_ROLE = "COURIER";
    public static final String CUSTOMER_ROLE = "CUSTOMER";

    private Integer userRoleId;
    private String role;

    public Role() {
    }

    public Role(String role) {
        this.role = role;
    }

    @Id
    @GeneratedValue
    @Column(name = "user_role_id",
            unique = true, nullable = false)
    public Integer getUserRoleId() {
        return this.userRoleId;
    }

    public void setUserRoleId(Integer userRoleId) {
        this.userRoleId = userRoleId;
    }


    @Column(name = "role", nullable = false, length = 45)
    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public static boolean isValidRole(Role role) {
        if (role.getRole().equals(Role.ADMIN_ROLE) ||
                role.getRole().equals(Role.OPERATOR_ROLE) ||
                role.getRole().equals(Role.CUSTOMER_ROLE) ||
                role.getRole().equals(Role.COURIER_ROLE)) {
            return true;
        } else {
            return false;
        }
    }
}