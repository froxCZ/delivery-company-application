package com.wa2.common.model.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import com.wa2.common.model.Group;
import org.hibernate.validator.constraints.NotEmpty;

import com.wa2.common.model.Member;
import com.wa2.common.model.product.Product;

@SuppressWarnings("serial")
@Entity
@XmlRootElement
@Table(name="consignments")
@NamedQueries({
		@NamedQuery(name = "Delivery.findDeliveredNotAccountedOrdersOfMessengerInPeriod", query = "SELECT d FROM Delivery d WHERE d.courier = :courier " +
				"AND d.pickupPoint.dateFrom > :fromDate AND d.deliveryPoint.dateTo < :toDate AND d.state=com.wa2.common.model.order.OrderState.DELIVERED AND d.isAccounted = false"),
		@NamedQuery(name = "Delivery.findCompletedOrdersInPeriod", query = "SELECT d FROM Delivery d WHERE " +
				"d.pickupPoint.dateFrom > :fromDate AND d.deliveryPoint.dateTo < :toDate"),
        @NamedQuery(name = "Delivery.findByCode", query = "SELECT d FROM Delivery d WHERE d.code = :code")
})
public class Delivery implements Serializable {

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@NotEmpty
	//@Column(unique=true, nullable=false)
	@Column(nullable=false)
	private String code;
	
	@NotNull
	private BigDecimal price;
		
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="consignment_id")
	private List<Package> packages;

	@ManyToOne()
	@JoinColumn(name="product_id")
	private Product product;

	@ManyToOne()
	@JoinColumn(name="courier_id")
	private Member courier;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="pickup_point_id")
	private OrderPoint pickupPoint;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="delivery_point_id")
	private OrderPoint deliveryPoint;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="group_id")
	private Group group;

	@Column
	@Enumerated(EnumType.STRING)
	private OrderState state;

	/**
	 * true when this order is was processed and accounted by the turnover batch.
	 */
	@Column(name = "is_accounted")
	@NotNull
	private Boolean isAccounted = false;

//	@Column(name= "invoiced")
//	private Boolean invoiced;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public List<Package> getPackages() {
		return packages;
	}

	public void setPackages(List<Package> packages) {
		this.packages = packages;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public OrderPoint getPickupPoint() {
		return pickupPoint;
	}

	public void setPickupPoint(OrderPoint pickupPoint) {
		this.pickupPoint = pickupPoint;
	}

	public OrderPoint getDeliveryPoint() {
		return deliveryPoint;
	}

	public void setDeliveryPoint(OrderPoint deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}

	public Member getCourier() {
		return courier;
	}

	public void setCourier(Member courier) {
		this.courier = courier;
	}

	public OrderState getState() {
		return state;
	}

	public void setState(OrderState state) {
		this.state = state;
	}

	public Boolean isAccounted() {
		return isAccounted;
	}

	public void setIsAccounted(Boolean isAccounted) {
		this.isAccounted = isAccounted;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	@Override
	public String toString() {
		return "Delivery{" +
				"id=" + id +
				", code='" + code + '\'' +
				", price=" + price +
				", messenger =" + courier +
				'}';
	}
}
