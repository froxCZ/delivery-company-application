package com.wa2.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class Util {

    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

    public Date stringToDate(String dateStr) {
        try {
            return dateFormatter.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String dateToDateTimeString(Date created) {
        if (created == null) {
            return null;
        } else {
            return dateTimeFormatter.format(created);
        }
    }
}
